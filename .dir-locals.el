((nil . (
  (org-publish-project-alist .
   (("org-gitlab-publish"
          ;; Path to your org files.
          :base-directory "~/Documents/mysite/"
          :base-extension "org"
          :publishing-directory "~/Documents/mysite/public/"
          :recursive t
          :exclude "org-html-themes/.*\\|man-org/man*"
          :publishing-function org-html-publish-to-html
          :headline-levels 4
          :auto-sitemap t                ; Generate sitemap.org automagically...
          :index-filename "sitemap.org"
          :index-title "Sitemap"
          :sitemap-filename "sitemap.org"  ; ... call it sitemap.org (it's the default)...
          :sitemap-title "Sitemap"         ; ... with title 'Sitemap'.
          :auto-preamble t
    )

    ("assets-gitlab-publish"
          :base-directory "~/Documents/mysite/"
          :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|php"
          :exclude "org-html-themes/.*\\|man-org/man*\\|public/*"
          :publishing-directory "~/Documents/mysite/public/"
          :recursive t
          :publishing-function org-publish-attachment)

    ("gitlab-publish" :components ("org-gitlab-publish" "assets-gitlab-publish"))))
  )
))
