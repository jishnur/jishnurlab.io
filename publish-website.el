;; Set up package repositories
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; Load the required packages
(require 'ox-publish)
(require 'org)

;; Ensure citeproc is available
(unless (package-installed-p 'citeproc)
  (package-refresh-contents)
  (package-install 'citeproc))

;; Load citeproc
(require 'citeproc)
;; Customize the HTML output
;;(setq org-html-validation-link nil            ;; Don't show validation link
;;      org-html-head-include-scripts nil       ;; Use our own scripts
;;      org-html-head-include-default-style nil ;; Use our own styles
;;     )

;; Define the publishing project
(setq org-publish-project-alist
      '(("org-gitlab-publish"
         :base-directory "~/Documents/mysite/"
         :base-extension "org"
         :publishing-directory "~/Documents/mysite/public/"
         :recursive t
         :exclude "org-html-themes/.*\\|man-org/man*"
         :publishing-function org-html-publish-to-html
         :headline-levels 4
         :auto-sitemap t
         :index-filename "sitemap.org"
         :index-title "Sitemap"
         :sitemap-filename "sitemap.org"
         :sitemap-title "Sitemap"
         :auto-preamble t)

        ("assets-gitlab-publish"
         :base-directory "~/Documents/mysite/"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|php"
         :exclude "org-html-themes/.*\\|man-org/man*\\|public/*"
         :publishing-directory "~/Documents/mysite/public/"
         :recursive t
         :publishing-function org-publish-attachment)

        ("gitlab-publish"
         :components ("org-gitlab-publish" "assets-gitlab-publish"))))

;; Generate the site output
(org-publish-all t)

(message "Build complete!")
